import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go
import typing as t
import logging
from tqdm.auto import tqdm
import plotly.express as px

PLOTLY_CONFIGS = {
    'scrollZoom': True,
    'editable': True,
    'toImageButtonOptions': {
        'format': 'png'
    }
}


def set_df_font_size(size_cell='2rem', size_head='2.5rem'):
    """Increase the fontsize of the displayed tables
    to the desired size in rem"""
    from IPython import display
    css = """
    <style>
    table.dataframe {    
        max-width: auto;
        }
    table.dataframe thead {
        font-size: """ + size_head + """;
    }
    table.dataframe tbody {
        font-size: """ + size_cell + """;
    }
    th.col_heading.level1 {
        font-size: 2rem;
    }
    </style>"""
    display.display_html(css, raw=True)


def hide_code_in_slideshow():
    """Hides code from jupyter notebook cell
    when used with RISE / Reveal. Credits:
    https://www.markroepke.me/posts/2019/06/05/tips-for-slideshows-in-jupyter.html
    """
    from IPython import display
    import binascii
    import os
    uid = binascii.hexlify(os.urandom(8)).decode()
    html = """<div id="%s"></div>
    <script type="text/javascript">
        $(function(){
            var p = $("#%s");
            if (p.length==0) return;
            while (!p.hasClass("cell")) {
                p=p.parent();
                if (p.prop("tagName") =="body") return;
            }
            var cell = p;
            cell.find(".input").addClass("hide-in-slideshow")
        });
    </script>""" % (uid, uid)
    display.display_html(html, raw=True)


def apply_root_style(mpl_colors=True):
    """Set the defaults of matplotlib to
    be similar to ROOT plots using a custom
    stylesheet.
    :param mpl_colors: enable the default color palette tab10 of matplotlib. Otherwise use the ROOT one"""
    plt.style.use(
        'https://gist.githubusercontent.com/themutt/34b9121f44a4a525734c9c7c8577233d/raw/946d7a234cd4bc41792d2c3248300e89074fbe75/rootlike.mplstyle'
    )
    if mpl_colors:
        mpl.rc('axes', prop_cycle=mpl.rcParamsDefault['axes.prop_cycle'])


def get(ldb, query, t1, t2):
    """Emulated pytimber.LoggingDB.get() with a small difference.
    It retrieves the last stored point before t1 and set it to t1. 
    It is useful to understand what was the value before the time of t1.
    :param ldb: instance of pytimber.LoggingDB class
    :param t1: str, pd.Timestamp, datetime.datetime 
    :param t2: str, pd.Timestamp, datetime.datetime 
    :return: a dictionary similar to the one of LoggingDB.get()
    :rtype: dict
    """
    t1, t2 = pd.to_datetime(t1), pd.to_datetime(t2)
    initial_points = ldb.get(query, t1, None, unixtime=False)
    final_points = ldb.get(query, t2, None, unixtime=False)
    middle_points = ldb.get(query, t1, t2, unixtime=False)
    res = {}
    for key, (time, value) in middle_points.items():
        times = np.array([t1.to_pydatetime()] + time.tolist())
        values = np.array(initial_points[key][1].tolist() + value.tolist())
        times = np.append(times, t2.to_pydatetime())
        values = np.append(values, values[-1])
        res[key] = (times, values)
    return res


def pplot(data,
          fig=None,
          layout=None,
          show=True,
          show_configs=None,
          renderer='webgl'):
    """Plot data retrieved from pytimber with plotly. Return a
    plotly.graph_objects.Figure instance"""
    if not fig:
        fig = go.Figure(layout=layout or go.Layout())
    for key, (time, value) in data.items():
        if renderer == 'webgl':
            scatter = go.Scattergl(x=time,
                                   y=value,
                                   mode='markers+lines',
                                   name=key,
                                   line_shape='hv')
        else:
            scatter = go.Scatter(x=time,
                                 y=value,
                                 mode='markers+lines',
                                 name=key,
                                 line_shape='hv')
        fig.add_trace(scatter)
    if show:
        if not show_configs:
            return fig.show(config=PLOTLY_CONFIGS)
        return fig.show(config=show_configs)
    return fig


def plot(data, ax=None, figsize=(8, 4), dates_interval=None, legend=True):
    """Plot data retrieved from get() function. If not passed, the function 
    returns an Axes instance of matplotlib.    
    :param ax: axes on where to plot the values
    :param figsize: Size of the figure in inches
    :param dates_interval: None or int indicates the interval in days between displaying of the dates on the plot
    :return: an instance of matplotlib.

    """
    if not ax:
        fig, ax = plt.subplots(figsize=figsize)
    for k, v in data.items():
        ax.step(*v, label=k, where='post')
    if dates_interval:
        ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(dates_interval))
    if legend:
        ax.legend()
    ax.figure.autofmt_xdate()

    return ax


def get_values(ldb, query, start, end, extend=False, step_extend='12h'):
    """Return a Series like object with time as index. NaNs values filled with ffill and bfill.
    Optionally enables extend to allow to retrieve data in case the period doesn't have any.
    The data will be retrieved in range wider by step_extend until a point is found. The 
    last point will be considered the point of that period.
    :param ldb: instance of pytimber.LoggingDB class
    :param query: string indicating the name to retrieve. Can use jolly % characters
    :param start: string or datetime like object indicating the start of the period to retrieve
    :param stop: string or datetime like object indicating the stop of the period to retrieve
    :param extend: bool indicating if the start period should be extended in case no points were found
    :param step_extend: string indicating how long the period should be extended in case extend is enabled
    :return: a dataframe with timestamp as index and columns as found values
    :rtype: pd.DataFrame"""
    start = pd.to_datetime(start)
    end = pd.to_datetime(end)
    df = pd.DataFrame()
    series = []
    data = ldb.get(query, start, end, unixtime=False)
    empty_retrieved_values = True
    # import ipdb; ipdb.set_trace()
    for key, (time, value) in data.items():
        if extend:
            attempts = 1
            while empty_retrieved_values:
                # Meaning that if found data at first try
                if len(time) and attempts == 1:
                    break
                # Meaning that if found data after few attemps
                # -> get only the last value
                elif len(time):
                    time = [time[-1]]
                    value = [value[-1]]
                    break
                previous = start - pd.Timedelta(step_extend) * attempts
                print('Extend enabled: no values found, extending from ',
                      start, ' to ', previous)
                extended_data = ldb.get(key, previous, end, unixtime=False)
                time, value = extended_data[key]
                attempts += 1

        series.append(pd.Series(value, index=time, name=key))
    df = pd.DataFrame(series).T.fillna(method='pad').fillna(method='bfill')
    return df


def plot_values(ldb,
                values,
                start,
                stop,
                dates_interval=None,
                configs={},
                ax=None):
    """Return fig, ax of values retrieved from timber in
    certain period
    :param ldb: instance of pytimber.LoggingDB()
    :param values: str or list of the values to pass to the getAligned() function
    :param start: str or datetime like object
    :param stop: str or datetime like object
    :param dates_interval: None or int indicates the interval in days between displaying of the dates on the plot
    :param configs: None or dict additional configs that can be passed to plt.rc() function
    :param ax: axes on where to plot the values
    :return: tuple of fig, ax and dataframe
    :rtype: tuple"""
    data = ldb.get(values, start, stop, unixtime=False)
    if not ax:
        fig, ax = plt.subplots()
    else:
        fig = ax.figure
    for key, value in data.items():
        ax.plot(value[0], value[1], label=key)
    ax.legend(loc='best', fontsize=7)
    plt.rc(configs)
    fig.autofmt_xdate()
    ax.tick_params(axis='x', which='both', labelsize=5)
    if dates_interval:
        ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(dates_interval))
    return fig, ax


def format_columns(df, format_map=None):
    """Apply the formatting options to the given dataframe
    using the format. Format is a dict containing key-> column
    and value -> the string that would be passed to str.format
    :param df: pd.DataFrame the dataframe to be styled
    :param format_map: dict the dictionary to be used to format the df
    :return: pd.formats.style.Styler the styler associated to the dataframe
    """
    if not format_map:
        format_map = {
            ('in_flow', 'mean'): '{:.0f}',
            ('in_flow', 'std'): '±{:.0f} ln/h',
            ('in_flow', 'ratio'): '{:.0%}',
            ('out_flow', 'mean'): '{:.0f}',
            ('out_flow', 'std'): '±{:.0f} ln/h',
            ('out_flow', 'ratio'): '{:.0%}',
            ('loss_rate', 'mean'): '{:.0f}',
            ('loss_rate', 'std'): '±{:.0f} ln/h',
            ('loss_rate', 'ratio'): '{:.0%}',
            ('in_pressure', 'mean'): '{:.2f}',
            ('in_pressure', 'std'): '±{:.2f} mbar',
            ('in_pressure', 'ratio'): '{:.0%}',
            ('in_pressure', 'mean'): '{:.2f}',
            ('in_pressure', 'std'): '±{:.2f} mbar',
            ('in_pressure', 'ratio'): '{:.0%}',
            ('det_pressure', 'mean'): '{:.2f}',
            ('det_pressure', 'std'): '±{:.2f} mbar',
            ('det_pressure', 'ratio'): '{:.0%}',
            ('out_pressure', 'mean'): '{:.2f}',
            ('out_pressure', 'std'): '±{:.2f} mbar',
            ('out_pressure', 'ratio'): '{:.0%}',
            ('reg_valve', 'mean'): '{:.1f}',
            ('reg_valve', 'std'): '±{:.1f} %',
            ('reg_valve', 'ratio'): '{:.0%}',
        }

    return df.style.format(format_map).set_table_attributes(
        'class="dataframe"')


def color_cells(df, row, col, color='orange'):
    """Color the cells of a dataframe or a Styler object.
    The cells are specified by rows and columns index. A color
    can be passed as well.
    :param df: pd.DataFrame the dataframe to color
    :param row: int the index number of the row
    :param col: int the index number of the row
    :param color: str the color to apply
    :return: pd.io.formats.Styler the styled dataframe"""
    def highlight(x, row, col, color):
        color = f'background-color: {color}'
        df1 = pd.DataFrame('', index=x.index, columns=x.columns)
        df1.iloc[row, col] = color
        return df1

    if isinstance(df, pd.core.frame.DataFrame):
        obj = df.style
    elif isinstance(df, pd.io.formats.style.Styler):
        obj = df
    else:
        raise Exception('Only Dataframe or Styler are supported.')
    return obj.apply(highlight, axis=None, row=row, col=col, color=color)


def data_to_df(data):
    """Transform the result of a pytimber query into a time
    series dataframe. Data should be a dict where keys are the
    name of the variables and values are tuples (times, series)
    where 'times' and 'series' are both numpy ndarray.
    The returned dataframe has the union of all the timestamps
    contained in data and one variable for each column. The 
    missing values for a variables are filled with 'ffill'

    :param data: dict the data returned by  pytimber.get() query
    :return: pd.DataFrame a handy dataframe for time series operation
    """
    df = pd.DataFrame()
    for name, (times, values) in data.items():
        serie = pd.Series(values, index=times, name=name)
        df = pd.merge(df,
                      serie,
                      left_index=True,
                      right_index=True,
                      how='outer')
    return df.fillna(method='ffill')


def pollute_spark():
    """Set up 'sc' and 'spark' as two global variables"""
    global sc, spark
    sc = None
    spark = None


def start_spark(verbose=True):
    """Start a spark session and set up two variables to be used
    for access to the NXCALS cluster: sc and spark. This variables can later
    be used to query for WINCCOA data. Snippet from:
    https://swan-community.web.cern.ch/t/connect-to-spark-cluster-on-startup/348/4?u=grigolet
    """
    pollute_spark()
    import logging
    logger = logging.getLogger(__name__)
    global sc, spark
    try:
        spark.stop()
    except Exception as e:
        pass
    if not sc or not spark:
        # Manual spark configuration to execute notebook outside of SWAN service

        import os
        import random
        import subprocess

        from pyspark import SparkContext, SparkConf
        from pyspark.sql import SparkSession

        if "SPARK_PORTS" in os.environ:
            ports = os.getenv("SPARK_PORTS").split(",")
        else:
            ports = [
                random.randrange(5001, 5300),
                random.randrange(5001, 5300),
                random.randrange(5001, 5300)
            ]

        # Spark Config
        logger.debug('Starting nxcals jar')
        nxcals_jars = subprocess.run(
            ['ls $LCG_VIEW/nxcals/nxcals_java/* | xargs | sed -e "s/ /:/g"'],
            shell=True,
            stdout=subprocess.PIPE,
            env=os.environ).stdout.decode('utf-8')
        logger.info('Setting up spark configs')
        conf = SparkConf()
        conf.set('spark.master', 'yarn')
        conf.set("spark.logConf", True)
        conf.set("spark.driver.host", os.environ.get('SERVER_HOSTNAME'))
        conf.set("spark.driver.port", ports[0])
        conf.set("spark.blockManager.port", ports[1])
        conf.set("spark.ui.port", ports[2])
        conf.set('spark.executorEnv.PYTHONPATH', os.environ.get('PYTHONPATH'))
        conf.set('spark.executorEnv.LD_LIBRARY_PATH',
                 os.environ.get('LD_LIBRARY_PATH'))
        conf.set('spark.executorEnv.JAVA_HOME', os.environ.get('JAVA_HOME'))
        conf.set('spark.executorEnv.SPARK_HOME', os.environ.get('SPARK_HOME'))
        conf.set('spark.executorEnv.SPARK_EXTRA_CLASSPATH',
                 os.environ.get('SPARK_DIST_CLASSPATH'))
        conf.set('spark.driver.extraClassPath', nxcals_jars)
        conf.set('spark.executor.extraClassPath', nxcals_jars)
        conf.set(
            'spark.driver.extraJavaOptions',
            "-Dlog4j.configuration=file:/eos/project/s/swan/public/NXCals/log4j_conf -Dservice.url=https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093 -Djavax.net.ssl.trustStore=/etc/pki/tls/certs/truststore.jks -Djavax.net.ssl.trustStorePassword=password"
        )

        sc = SparkContext(conf=conf)
        spark = SparkSession(sc)


def query(query_variables: t.Union[str, t.List], tstart: t.Union[int,
                                                                 pd.Timestamp],
          tstop: t.Union[int, pd.Timestamp]) -> pd.DataFrame:
    """Query variables using spark and NXCALS cluster. The result is returned
    by default as a pandas DataFrame.

    :param query_variables: a string or a list of string of the variables to 
        search. Jolly characters '%' and '_' are accepted
    :param tstart: string or datetime like of the start for the query
    :param tstop: string or datetime like of the stop 
    :returns: None or pd.DataFrame
        returns None if now rows are found. It returns a dataframe with an index
        set to the timestamp (in datetime like format) and N columns where N
        is the number of found variables. The timestamps are a union of all
        the timestamps of the single variables and NaN values might be present.

    """
    from cern.nxcals.api.extraction.data.builders import DataQuery  # for DataQuery
    # for converting timestamps
    from pyspark.sql.functions import pandas_udf, PandasUDFType
    from pyspark.sql import functions as f  # generic spark sql useful functions
    from pyspark.sql import types as t  # generic classes represting data types
    from pyspark.context import SparkContext
    from pyspark.sql.session import SparkSession
    sc = SparkContext.getOrCreate()
    spark = SparkSession(sc)

    # Defining UDF function converting timestamp stored as long to Human readable version
    @pandas_udf('timestamp', PandasUDFType.SCALAR)
    def to_stamp(stamps):
        # note: use utc=True to avoid pandas using local timezone and getting missing dates errors
        return pd.to_datetime(stamps, unit='ns', utc=True)

    to_search = []
    if isinstance(query_variables, str):
        to_search = [query_variables]
    elif isinstance(query_variables, list):
        for item in query_variables:
            to_search.append(item)
    else:
        raise ValueError(
            f'query_variables should be str or list but the type was {type(query_variables)}'
        )

    df = DataQuery.builder(spark).byEntities().system('WINCCOA') \
        .startTime(tstart).endTime(tstop)
    for variable in to_search:
        df = df.entity().keyValueLike('variable_name', variable)
    df = df.buildDataset()
    length = df.count()
    if not length:
        logging.warning('No rows found')
        return
    logging.info(f'Found {length} rows')

    df = df.groupby('timestamp').pivot('variable_name').agg(f.last('value').alias('value')) \
        .withColumn('time', to_stamp(f.col('timestamp'))) \
        .drop('timestamp') \
        .sort('time')

    pdf = df.toPandas().set_index('time')
    return pdf


def grouped_query(
        query_variables: t.Union[str, t.List],
        tstart: t.Union[int, pd.Timestamp],
        tstop: t.Union[int, pd.Timestamp],
        time_interval: str = '1 day',
        disable_progress: bool = False,
        aggregation_function: t.Union[t.Callable,
                                      str] = 'last') -> pd.DataFrame:
    """Query variables using spark and NXCALS cluster with a grouped aggregation. 
    The result is returned by default as a pandas DataFrame. The aggregation
    is done on a time interval defined by `time_interval` and the aggregation
    function is an average. This is useful for retrieving historical data
    without having too many points.

    :param query_variables: a string or a list of string of the variables to 
        search. Jolly characters '%' and '_' are accepted
    :param tstart: string or datetime like of the start for the query
    :param tstop: string or datetime like of the stop 
    :param time_interval: str pyspark string for group by time aggregation
    :param disable_progress bool: wether to disable tqdm progress bar
    :param aggregation_function callable or str: A function or a string to be used to aggregated
        the results over a time window. Supported strings: 'last', 'min', 'max', 'mean', 'avg', 'count'
    :returns: None or pd.DataFrame
        returns None if now rows are found. It returns a dataframe with an index
        set to the timestamp (in datetime like format) and N columns where N
        is the number of found variables. The timestamps are a union of all
        the timestamps of the single variables and NaN values might be present.

    """
    from cern.nxcals.api.extraction.data.builders import DataQuery  # for DataQuery
    # for converting timestamps
    from pyspark.sql.functions import pandas_udf, PandasUDFType
    from pyspark.sql import functions as f  # generic spark sql useful functions
    from pyspark.sql import types as t  # generic classes represting data types
    from pyspark.context import SparkContext
    from pyspark.sql.session import SparkSession
    sc = SparkContext.getOrCreate()
    spark = SparkSession(sc)

    statistics_map = {
        'last': f.last,
        'min': f.min,
        'max': f.max,
        'mean': f.mean,
        'avg': f.avg,
        'count': f.count
    }
    if isinstance(aggregation_function, str):
        statistic_function = statistics_map[aggregation_function]
    elif callable(aggregation_function):
        statistic_function = aggregation_function

    # Defining UDF function converting timestamp stored as long to Human readable version
    @pandas_udf('timestamp', PandasUDFType.SCALAR)
    def to_stamp(stamps):
        # note: use utc=True to avoid pandas using local timezone and getting missing dates errors
        return pd.to_datetime(stamps, unit='ns', utc=True)

    tstart = pd.to_datetime(tstart)
    tstop = pd.to_datetime(tstop)

    to_search = []
    if isinstance(query_variables, str):
        to_search = [query_variables]
    elif isinstance(query_variables, list):
        for item in query_variables:
            to_search.append(item)
    else:
        raise ValueError(
            f'query_variables should be str or list but the type was {type(query_variables)}'
        )

    with tqdm(total=12, disable=disable_progress) as pbar:
        df = DataQuery.builder(spark).byEntities().system('WINCCOA') \
            .startTime(tstart).endTime(tstop)
        pbar.update(1)

        for variable in to_search:
            df = df.entity().keyValueLike('variable_name', variable)
        pbar.update(1)

        df = df.buildDataset()
        pbar.update(1)
        length = df.count()
        pbar.update(1)

        if not length:
            logging.warning('No rows found')
            return
        logging.info(f'Found {length} rows')

        df = df.withColumn('time', to_stamp(f.col('timestamp')))
        pbar.update(1)

        df = df.groupby(f.window('time', time_interval))
        pbar.update(1)

        df = df.pivot('variable_name')
        pbar.update(1)

        df = df.agg(statistic_function('value').alias('value'))
        pbar.update(1)

        columns = ['window.start'] + \
            [f'`{col}`' for col in df.columns if 'window' not in col]
        df = df.select(columns)
        pbar.update(1)

        df = df.withColumnRenamed('start', 'time')
        pbar.update(1)

        df = df.sort('time')
        pbar.update(1)

        pdf = df.toPandas().set_index('time')
        pbar.update(1)

    return pdf


def multiple_axes_plotly(df,
                         width=2500,
                         height=800,
                         step=0.03,
                         template='plotly',
                         additional_config={},
                         render=True,
                         render_config=None):
    """Create a plotly figure to resemble winccoa trend plot. 

    :param width int: The width in pixel of the figure
    :param height int: The height in pixel of the figure
    :param step float: The step to separate the vertical Y axes
    :param template str or object: The styling template for the plot 
    :param additional_config dict: additional config to pass to the layout of the plotly figure
    :param render bool=True: directly render the plot by calling the .show() method on figure object
    :param render_config dict: a dict to be passed to the .show() method if render=True

    :returns plotly.graph_objects.Figure the figure object
    """
    filled = df.fillna(method='ffill')
    colors = px.colors.qualitative.Safe
    ncolors = len(colors)
    fig = go.Figure()
    ncols = len(df.columns)
    layout = {
        'xaxis': {
            'domain': [step * (ncols - 1), 1]
        },
        'width': width,
        'height': height,
    }

    for ix, col in enumerate(df.columns[::-1], start=1):
        data = filled.loc[:, col]
        range_diff = data.max() - data.min()
        range_min = data.min() - ((range_diff + range_diff / 5) * (ncols - ix))
        range_max = data.max() + range_diff * ix
        layout_config = {}
        x = data.index
        y = data.values
        fig.add_trace(
            go.Scatter(x=x,
                       y=y,
                       name=col,
                       yaxis=f"y{ix}",
                       line={
                           'color': colors[ix % ncolors],
                           'shape': 'hv'
                       }))

        y_string = str(ix) if ix > 1 else ''
        layout_config = {
            'title': col,
            'titlefont': {
                'color': colors[ix % ncolors]
            },
            'tickangle': -90,
            'tickfont': {
                'color': colors[ix % ncolors]
            },
            'tickcolor': colors[ix % ncolors],
            'side': 'left',
            'position': step * (ix - 1),
            'title_standoff': 0,
            'ticks': 'outside',
            'zeroline': False,
            'showgrid': False,
            'range': [range_min, range_max]
        }
        if ix > 1:
            layout_config['overlaying'] = 'y'
        layout[f'yaxis{y_string}'] = layout_config

    layout.update(additional_config)
    fig.update_layout(layout)

    if render:
        config = render_config or PLOTLY_CONFIGS
        return fig.show(config=config)

    return fig
