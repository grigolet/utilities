from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='utilities',
      version='0.1',
      description='Personal utilities for different purposes',
      url='https://gitlab.cern.ch/grigolet/utilities',
      author='Gianluca Rigoletti',
      author_email='gianluca.rigoletti@cern.ch',
      license='MIT',
      packages=find_packages(),
      zip_safe=True,
      install_requires=['matplotlib',
                        'pandas', 'tqdm'])
